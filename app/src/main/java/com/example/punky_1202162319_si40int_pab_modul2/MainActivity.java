package com.example.punky_1202162319_si40int_pab_modul2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.text.DateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    TextView isiSaldo, wakt, inWw, inTg, tgll, tgg, wkk, txtiket, saldo, selesai;
    Button topup, tumbas;
    String harga, city;
    int select_tujuan;
    private int jam, menit, day, month, year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        saldo = findViewById(R.id.isiSaldo);

        Intent i = getIntent();
        String in = i.getStringExtra("done");
        if(in != null) {
            selesai = findViewById(R.id.selesai);
            selesai.setVisibility(View.VISIBLE);
            String sisa = String.valueOf(i.getIntExtra("sisa",0));
            saldo.setText(sisa);
        }
        Spinner spinner;
        ArrayAdapter<CharSequence> adapter;

        topup =findViewById(R.id.inputSaldo);
        topup.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final Dialog topup_dialog = new Dialog(MainActivity.this);

                topup_dialog.setContentView(R.layout.topup);
                topup_dialog.setTitle("ISI SALDO ANDA!");

                final EditText isi_saldo = topup_dialog.findViewById(R.id.isi_saldo);
                Button cancel = topup_dialog.findViewById(R.id.btnCancel);
                final Button btnTop = topup_dialog.findViewById(R.id.btnYes);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        topup_dialog.dismiss();
                    }
                });

                btnTop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        isiSaldo = findViewById(R.id.isiSaldo);

                        int tmbhan = Integer.parseInt(isi_saldo.getText().toString());
                        int saldo_crnt =Integer.parseInt(isiSaldo.getText().toString());
                        int ttl = saldo_crnt + tmbhan;

                        isiSaldo.setText(String.valueOf(ttl));

                        topup_dialog.dismiss();
                    }
                });


                topup_dialog.show();
            }
        });

        tgg =findViewById(R.id.inTanggal);
        tgg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog DatePick = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        tgg.setText(dayOfMonth + " - "+ month +" - "+ year);
                    }
                },year,month,day);
                DatePick.show();
            }
        });


        tgll =findViewById(R.id.inTanggal2);
        tgll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog DatePick = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        tgll.setText(dayOfMonth + " - "+ month +" - "+ year);
                    }
                },year,month,day);
                DatePick.show();
            }
        });

        spinner = findViewById(R.id.btnspinner);
        adapter = ArrayAdapter.createFromResource(this, R.array.gabungan, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                select_tujuan = parent.getSelectedItemPosition();

                String[] biaya_array = getResources().getStringArray(R.array.harga_array);
                harga = String.valueOf(biaya_array[select_tujuan]);

                String[] kota_array = getResources().getStringArray(R.array.tujuan_array);
                city = String.valueOf(kota_array[select_tujuan]);

                Toast.makeText(getBaseContext(), city, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        wakt = findViewById(R.id.inWaktu2);
        wakt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                jam = c.get(Calendar.HOUR_OF_DAY);
                menit = c.get(Calendar.MINUTE);

                TimePickerDialog TimePick = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        wakt.setText(hourOfDay + " : " + minute);
                    }
                },jam, menit, true);
                TimePick.show();

            }
        });

        wkk = findViewById(R.id.inWaktu1);
        wkk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                jam = c.get(Calendar.HOUR_OF_DAY);
                menit = c.get(Calendar.MINUTE);

                TimePickerDialog TimePick = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        wkk.setText(hourOfDay + " : " + minute);
                    }
                },jam, menit, true);
                TimePick.show();

            }
        });


        final Switch swtch = findViewById(R.id.switch1);
        swtch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(swtch.isChecked()){
                    inWw = findViewById(R.id.inWaktu1);
                    inTg = findViewById(R.id.inTanggal2);

                    inWw.setVisibility(View.VISIBLE);
                    inTg.setVisibility(View.VISIBLE);
                } else {
                    inWw.setVisibility(View.INVISIBLE);
                    inTg.setVisibility(View.INVISIBLE);
                }
            }
        });

        tumbas = findViewById(R.id.btnBeli);
        tumbas.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                txtiket = findViewById(R.id.inTikett);
                int tiket = Integer.parseInt(txtiket.getText().toString());
                int biaya_jalan = Integer.parseInt(harga);
                int total_biaya = tiket * biaya_jalan;

                int saldo_curr = Integer.parseInt(isiSaldo.getText().toString());

                if (saldo_curr < total_biaya) {
                    Toast.makeText(MainActivity.this,"Saldo Anda Tidak Cukup \n Silahkan Top Up Terlebih Dahulu",
                            Toast.LENGTH_SHORT).show();
                } else if (total_biaya == 0) {
                    Toast.makeText(MainActivity.this,"Isi Form Dengan Lengkap",Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, CheckoutActivity.class);
                    Bundle info = new Bundle();
                    String tgl_brkt = tgg.getText().toString();
                    String jam_brkt = wakt.getText().toString();



                    info.putString("tujuan",city);
                    info.putString("tgl_brkt",tgl_brkt);
                    info.putString("jam_brkt",jam_brkt);
                    info.putInt("saldo",saldo_curr);

                    if(swtch.isChecked()) {
                        info.putInt("swtch",1);
                        info.putString("tgl_blk",inTg.getText().toString());
                        info.putString("jam_blk",inWw.getText().toString());
                        total_biaya += total_biaya;
                    }

                    info.putString("total", String.valueOf(total_biaya));
                    intent.putExtras(info);
                    MainActivity.this.startActivity(intent);
                }


            }
        });

    }
}
package com.example.punky_1202162319_si40int_pab_modul2;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;


public class SplashActivity extends Activity {
        @Override
        protected void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash);
            Thread thread = new Thread(){
                public void run() {
                    try{
                        sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        startActivity(new Intent( SplashActivity.this, MainActivity.class));
                        finish();
                    }
                }
            };
            thread.start();
        }
}

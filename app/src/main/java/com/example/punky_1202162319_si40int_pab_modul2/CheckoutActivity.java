package com.example.punky_1202162319_si40int_pab_modul2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheckoutActivity extends Activity {
    TextView tujuan, brgkt, plng, total, plgTxt;
    Button conf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_activity);

        final Intent i = getIntent();
        final Bundle info = i.getExtras();

        final int saldo = info.getInt("saldo");

        tujuan = findViewById(R.id.tujuan);
        brgkt = findViewById(R.id.brkt);
        plng = findViewById(R.id.pulang);
        plgTxt = findViewById(R.id.txtPulang);
        total = findViewById(R.id.total);

        if (info.getInt("swtch") == 1) {
            plng.setVisibility(View.VISIBLE);
            plgTxt.setVisibility(View.VISIBLE);
        }

        String brktTxt = info.getString("tgl_brkt") + " - " + info.getString("jam_brkt");
        String plgTxt = info.getString("tgl_blk") + " - " + info.getString("jam_blk");

        tujuan.setText(info.getString("tujuan"));
        brgkt.setText(brktTxt);
        plng.setText(plgTxt);
        total.setText(info.getString("total"));

        conf = findViewById(R.id.btnBayar);
        conf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int total_biaya = Integer.parseInt(total.getText().toString());
                int sisa_saldo = saldo - total_biaya;

                Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
                intent.putExtra("done", "yes");
                intent.putExtra("sisa", sisa_saldo);
                CheckoutActivity.this.startActivity(intent);
            }
        });

    }
}
